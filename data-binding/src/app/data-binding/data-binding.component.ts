import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  url: string = 'http://kelvinromero.com'
  urlImagem = 'https://picsum.photos/200/300'

  valorAtual: String;
  valorSalvo: String;

  isMouseOver: boolean = false;

  botaoClicado(){
    alert("Botão clicado!")
  }

  onKeyUp(evento: KeyboardEvent){
      this.valorAtual = (<HTMLInputElement>evento.target).value;
  }

  salvarValor(valor){
    this.valorSalvo = valor;
  }

  onMouseOverOut(){
    this.isMouseOver = !this.isMouseOver;
  }
  constructor() { }

  ngOnInit() {
  }



}
